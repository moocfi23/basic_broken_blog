from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class BlogPost(models.Model):
    '''
    Single post entry in the blog
    '''
    post_visibility = (
        ("private", "private"),
        ("public", "public"),
    )

    title = models.CharField(max_length=150)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    blog_entry = models.TextField()
    post_date = models.DateTimeField(auto_now_add=True)
    status = models.CharField(choices=post_visibility, default='private', max_length=20)

