from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import BlogPost
from django.db.models import Q

@login_required
def blogView(request):
    if request.method == "GET":
        # The blog post view displays all the posts, including the ones marked "private"
        # that should be visible only to the author of the blog entry. To fix this issues make sure that your
        # list of blog entries returns only those private requests that are connected with the author currently
        # logged in like that using Q functionality from django:
        #  blog_posts = BlogPost.objects.all().filter(Q(status="public") | Q(status="private", author=request.user))
        blog_posts = BlogPost.objects.all()
        return render(request, 'pages/all_posts.html', {"blog_posts": blog_posts})

@login_required
def addPost(request):
    '''
    Adding one single post by registered user
    :param request:
    :return:
    '''

    if request.method == "POST":
        try:
            print(request.POST)
            title = request.POST.get("title")
            blog_entry = request.POST.get("blog_entry")
            author = User.objects.get(username=request.user)
            status = request.POST.get("visibility")
            new_post = BlogPost(title=title, blog_entry=blog_entry, author=author, status=status)
            new_post.save()
        except:
            return redirect("/add")
    return render(request, 'pages/add.html')

def registerView(request):
    '''
    possibility for new person to register as an author on the website
    :param request:
    :return:
    '''

    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            username = form.clean_username()
            password = form.clean_password2()
            new_user = User(username=username)
            new_user.set_password(password)
            new_user.save()
            return redirect('/')
        else:
            return redirect("/register")

    else:
        form = UserCreationForm()

    return render(request, 'pages/register.html', {'form': form})

